# hashicorp-cluster #

A simple setup for Hashicorp tools including:

- [x] Consul
- [x] Nomad
- [ ] Vault
- [ ] Some custom app to be deployed