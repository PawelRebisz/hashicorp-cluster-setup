VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    def create_configured_host(config, hostname, index, hostIp, consulInitJson, nomadInitHcl)
        config.vm.define hostname do |node|
            # General details about vm and its name
            node.vm.box = "generic/centos7"
            node.vm.hostname = hostname

            # Networking, including NAT, visible port in main host and private network for the whole cluster
            node.vm.network :forwarded_port, guest: 22, host: "1#{index}022", host_ip: "127.0.0.1"
            node.vm.network :forwarded_port, guest: 80, host: "1#{index}080", host_ip: "127.0.0.1"
            node.vm.network :forwarded_port, guest: 8080, host: "1#{index}088", host_ip: "127.0.0.1"
            if hostname == 'consul-server'
                # This is port forwarding for 'consul server' UI (http://localhost:8500/ui/#/dc1/services/consul)
                node.vm.network :forwarded_port, guest: 8500, guest_ip: "#{hostIp}", host: 8700, host_ip: "127.0.0.1"
                # This is port forwarding for 'nomad server' UI (http://localhost:4646/ui/)
                node.vm.network :forwarded_port, guest: 4646, host: 4646, host_ip: "127.0.0.1"
            end
            node.vm.network :private_network, ip: hostIp, virtualbox__intnet: "node-cluster"
            node.ssh.forward_agent = true

            # Additional configuration for created vm, ie. max. RAM consumption
            node.vm.provider :virtualbox do |v|
                v.customize ["modifyvm", :id, "--memory", 1024]
            end

            # Attached volumes, which will be available in the vm
            node.vm.synced_folder "./", "/vagrant"

            # Main setup of consul in the created vm
            node.vm.provision :shell, path: "./vagrant_provision.sh"

            # Start of the consul process via RHEL services
            node.vm.provision "shell", inline: "echo '#{consulInitJson}' > /etc/systemd/system/consul.d/init.json"
            node.vm.provision "shell", inline: "service consul start"

            # Start of the nomad process via RHEL services
            node.vm.provision "shell", inline: "echo '#{nomadInitHcl}' > /etc/systemd/system/nomad.d/nomad-agent-configuration.hcl"
            node.vm.provision "shell", inline: "service nomad start"
        end
    end

    # Materialize the whole setup
    serverIp = ""
    for i in 0..2
        hostname = ""
        hostIp = "192.168.0.10#{i}"
        if i == 0
            # First host will be the 'consul server'
            hostname="consul-server"
            serverIp = hostIp

            consulServerInitJson = %( {
                "datacenter": "warsaw-1",
                "advertise_addr": "#{hostIp}",
                "client_addr": "#{hostIp}",
                "data_dir": "/tmp/consul",
                "server": true,
                "log_level": "INFO",
                "ui": true,
                "bootstrap_expect": 1
            } )
            nomadServerInitHcl = %(
                datacenter         = "warsaw-1"
                data_dir           = "/tmp/nomad"
                log_level          = "INFO"
                advertise {
                  rpc              = "#{hostIp}:4647"
                  serf             = "#{hostIp}:4648"
                }
                server {
                  enabled          = true
                  bootstrap_expect = 1
                }
                consul {
                  address          = "#{hostIp}:8500"
                }
            )
            create_configured_host config, "consul-server", i, hostIp, consulServerInitJson, nomadServerInitHcl
        else
            # Further hosts will be simple noges
            hostname="node-#{i}"

            consulClientInitJson = %( {
                "datacenter": "warsaw-1",
                "advertise_addr": "#{hostIp}",
                "data_dir": "/tmp/consul",
                "server": false,
                "log_level": "INFO",
                "retry_join": ["#{serverIp}"]
            } )
            nomadClientInitHcl = %(
                datacenter         = "warsaw-1"
                data_dir           = "/tmp/nomad"
                log_level          = "INFO"
                advertise {
                  rpc              = "#{hostIp}:4647"
                  serf             = "#{hostIp}:4648"
                }
                client {
                  enabled          = true
                  servers          = [ "#{serverIp}" ]
                }
            )
            create_configured_host config, hostname, i, hostIp, consulClientInitJson, nomadClientInitHcl
        end
    end

end
