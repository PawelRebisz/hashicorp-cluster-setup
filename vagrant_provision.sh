#!/bin/bash

# Required for outside communication (despite the NAT existing, still proxy has to be set)
#export https_proxy=<something>
#export http_proxy=<something>
export no_proxy=localhost,127.0.0.1
#export ftp_proxy=<something>

NOMAD_VERSION=0.8.1
CONSUL_VERSION=1.2.0
HOME_PATH="/home/node-cluster"

mkdir -p "${HOME_PATH}"
cd "${HOME_PATH}"

# Installation of docker
if [ ! -f /usr/bin/docker ]; then
    yum install -y yum-utils device-mapper-persistent-data  lvm2
    yum-config-manager  --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    yum install -y docker-ce
    systemctl start docker
fi

# Installation of consul
if [ ! -f /usr/local/bin/consul ]; then
    wget -q "https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip" -O consul.zip
    gunzip -d -S zip consul.zip

    # Make consul executable
    mv consul. consul
    chmod +x consul

    # Make consul accessible to RHEL service
    cp consul /usr/local/bin/

    # Preparation of RHEL service
    if [ ! -f /etc/systemd/system/consul.service ]; then
        cp /vagrant/rhel_consul.service /etc/systemd/system/consul.service
    fi
    if [ ! -d /etc/systemd/system/consul.d ]; then
        mkdir -p /etc/systemd/system/consul.d
    fi
fi

# Installation of nomad
if [ ! -f /usr/local/bin/nomad ]; then
    wget -q "https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip" -O nomad.zip
    gunzip -d -S zip nomad.zip

    # Make nomad executable
    mv nomad. nomad
    chmod +x nomad

    # Make nomad accessible to RHEL service
    cp nomad /usr/local/bin/

    if [ ! -f /etc/systemd/system/nomad.service ]; then
        cp /vagrant/rhel_nomad.service /etc/systemd/system/nomad.service
    fi
    if [ ! -d /etc/systemd/system/nomad.d ]; then
        mkdir -p /etc/systemd/system/nomad.d
    fi
fi

# Shutdown firewall
systemctl stop firewalld

